/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.airbabouche.compagnie.dao;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.swing.GroupLayout;
import javax.swing.JFrame;
import javax.swing.JTextField;
import org.bson.Document;

/**
 *
 * @author Slasherbane
 */
public class container extends JFrame {

    private javax.swing.JButton findButton;
    private javax.swing.JButton addBtn;
    private javax.swing.JButton updateBtn;
    private javax.swing.JButton deleteBtn;
    private javax.swing.JButton recupDataBtn;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;

    public void init(List<String> tableField, MongoDatabase db, String table) {

        MongoCollection<Document> data = db.getCollection(table);
        Iterator<Document> docs = data.find().iterator();
        List<JTextField> fields = new ArrayList<>();

        findButton = new javax.swing.JButton();
        addBtn = new javax.swing.JButton();
        updateBtn = new javax.swing.JButton();
        deleteBtn = new javax.swing.JButton();
        recupDataBtn = new javax.swing.JButton();
        jTextField1 = new javax.swing.JTextField();
        jTextField2 = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jLabel1 = new javax.swing.JLabel();
        jLabel1.setText("Administration " + table);

        jTextArea1.setText("");

        while (docs.hasNext()) {

            jTextArea1.setText(jTextArea1.getText() + " "
                    + "\n" + docs.next().toString());

        }

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        findButton.setText("Récup data");
        findButton.addActionListener((e) -> {
            Map<String, Object> map = new HashMap<>();
            Iterator<String> field = tableField.iterator();
            Iterator<JTextField> input = fields.iterator();
            while (field.hasNext() && input.hasNext()) {

                map.put(field.next(), input.next().getText());
            }
            Document document = new Document(map);
            Iterator<Document> responseData = data.find(document).iterator();
            while (responseData.hasNext()) {
                jTextArea1.setText("");
                jTextArea1.setText(jTextArea1.getText() + " " + responseData.next().toString());
            }

        });
        final String lightweight = jTextArea1.getText();

        addBtn.setText("Ajouter les données");
        addBtn.addActionListener((e) -> {

            Map<String, Object> map = new HashMap<>();
            Iterator<String> field = tableField.iterator();
            Iterator<JTextField> input = fields.iterator();
            while (field.hasNext() && input.hasNext()) {
                map.put(field.next(), input.next().getText());
            }
            Document document = new Document(map);
            data.insertOne(document);
            updateData(db, table);
        });

        updateBtn.setText("Mettre à jour les données");
        updateBtn.addActionListener((e) -> {
            Map<String, Object> map = new HashMap<>();
            Iterator<String> field = tableField.iterator();
            Iterator<JTextField> input = fields.iterator();
            String idfilterval = "";
            while (field.hasNext() && input.hasNext()) {
                String s = input.next().getText();
                String sfield = field.next();
                map.put(sfield, s);
                if (sfield.equals("id")) {
                    idfilterval = s;
                }
            }

            Document filter = new Document("id", idfilterval);
            Document document = new Document(map);
            Document f = new Document("$set", document);
            data.updateOne(filter, f);
            updateData(db, table);
        });

        deleteBtn.setText("Supprimer les données");

        deleteBtn.addActionListener((e) -> {
            Map<String, Object> map = new HashMap<>();
            Iterator<String> field = tableField.iterator();
            Iterator<JTextField> input = fields.iterator();
            String idfilterval = "";
            while (field.hasNext() && input.hasNext()) {
                String s = input.next().getText();
                String sfield = field.next();
                map.put(sfield, s);
                if (sfield.equals("id")) {
                    idfilterval = s;
                }
            }

            Document filter = new Document("id", idfilterval);

            data.deleteOne(filter);
            updateData(db, table);
        });
        recupDataBtn.setText("All data");
        recupDataBtn.addActionListener((e) -> {
            updateData(db, table);
        });

        jTextField1.setText("jTextField1");

        jTextField2.setText("jTextField2");

        jTextArea1.setColumns(40);
        jTextArea1.setRows(5);
        jScrollPane1.setViewportView(jTextArea1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());

        getContentPane().setLayout(layout);

        GroupLayout.SequentialGroup sq = layout.createSequentialGroup();
        GroupLayout.ParallelGroup pq = layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER);
        for (String s : tableField) {
            JTextField j = new javax.swing.JTextField();
            j.setText(s);
            fields.add(j);
            sq.addComponent(j, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE);
            sq.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED);
            pq.addComponent(j, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE);
        }

        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 500, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(findButton)
                                        .addComponent(addBtn)
                                        .addComponent(updateBtn)
                                        .addComponent(deleteBtn)
                                        .addComponent(recupDataBtn)
                                        .addGroup(sq))
                                .addContainerGap(32, Short.MAX_VALUE))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 280, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(116, 116, 116))
        );

        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addGap(15, 15, 15)
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(findButton)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(addBtn)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(updateBtn)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(deleteBtn)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(recupDataBtn)
                                .addGap(30, 30, 30)
                                .addGroup(pq)
                                .addGap(20, 20, Short.MAX_VALUE)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap())
        );

        pack();

    }

    private void updateData(MongoDatabase db, String table) {
        jTextArea1.setText("");
        Iterator<Document> localdocs = db.getCollection(table).find().iterator();
        while (localdocs.hasNext()) {

            jTextArea1.setText(jTextArea1.getText() + " "
                    + "\n" + localdocs.next().toString());

        }
    }
    
    public static void openNewContainer(String table, ArrayList<String> tableFields, MongoDatabase db, MongoClient mg) {
        container c = new container();

        c.init(tableFields, db, table);
        c.setVisible(true);
        //c.setDefaultCloseOperation();
        c.addWindowListener(new WindowAdapter() {
            //I skipped unused callbacks for readability

            @Override
            public void windowClosing(WindowEvent e) {
                mg.close();
                c.setVisible(false);
                c.dispose();
                System.exit(0);
            }
        });
    }
}
