/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classPackage;

import MongoDB.MongoDBConnexion;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import static com.mycompany.airbabouche.compagnie.dao.container.openNewContainer;
import java.util.ArrayList;

/**
 *
 * @author Flori
 */
public class VolClass {
    
    public ArrayList containerVol(){
        
        MongoDatabase db = new MongoDBConnexion().connexionDB();
        MongoClient clientDb = new MongoDBConnexion().getClient();
        MongoCollection<org.bson.Document> client = db.getCollection("vol");
        
        ArrayList<String> columnsVol = new ArrayList<>();
        columnsVol.add("id");
        columnsVol.add("place");
        columnsVol.add("intitule");
        columnsVol.add("aeroport_depart");
        columnsVol.add("aeroport_arriver");
        columnsVol.add("date_depart");
        columnsVol.add("date_arriver");
        columnsVol.add("id_compagnie");
        openNewContainer("vol", columnsVol, db, clientDb);
    
        return columnsVol;
    }
    
}
