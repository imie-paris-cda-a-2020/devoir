/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classPackage;

import MongoDB.MongoDBConnexion;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import static com.mycompany.airbabouche.compagnie.dao.container.openNewContainer;
import java.util.ArrayList;

/**
 *
 * @author Flori
 */
public class ClientClass {
    
    public ArrayList containerClient(){
        
        MongoDatabase db = new MongoDBConnexion().connexionDB();
        MongoClient clientDb = new MongoDBConnexion().getClient();
        MongoCollection<org.bson.Document> client = db.getCollection("client");
        
        ArrayList<String> columnsClient = new ArrayList<>();
        columnsClient.add("id");
        columnsClient.add("email");
        columnsClient.add("password");
        openNewContainer("client", columnsClient, db, clientDb);
    
        return columnsClient;
    }
}
