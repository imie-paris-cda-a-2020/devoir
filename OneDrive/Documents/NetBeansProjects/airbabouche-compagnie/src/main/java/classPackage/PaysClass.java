/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classPackage;

import MongoDB.MongoDBConnexion;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import static com.mycompany.airbabouche.compagnie.dao.container.openNewContainer;
import java.util.ArrayList;

/**
 *
 * @author Flori
 */
public class PaysClass {
    
    public ArrayList containerPays(){
        
        MongoDatabase db = new MongoDBConnexion().connexionDB();
        MongoClient clientDb = new MongoDBConnexion().getClient();
        MongoCollection<org.bson.Document> pays = db.getCollection("pays");
        
        
        ArrayList<String> columnsPays = new ArrayList<>();
        columnsPays.add("id");
        columnsPays.add("nom");
        openNewContainer("pays", columnsPays, db, clientDb);
        return columnsPays;
            
    }
}
