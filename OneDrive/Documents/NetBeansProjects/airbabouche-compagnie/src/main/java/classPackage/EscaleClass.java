/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classPackage;

import MongoDB.MongoDBConnexion;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import static com.mycompany.airbabouche.compagnie.dao.container.openNewContainer;
import java.util.ArrayList;

/**
 *
 * @author Flori
 */
public class EscaleClass {
        public ArrayList containerEscale(){
        
        MongoDatabase db = new MongoDBConnexion().connexionDB();
        MongoClient clientDb = new MongoDBConnexion().getClient();
        MongoCollection<org.bson.Document> escale = db.getCollection("escale");
        
        
        ArrayList<String> columnsEscale = new ArrayList<>();
        columnsEscale.add("id");
        columnsEscale.add("place");
        columnsEscale.add("intitule");
        columnsEscale.add("aeroport_depart");
        columnsEscale.add("aeroport_arriver");
        columnsEscale.add("date_depart");
        columnsEscale.add("date_arriver");
        columnsEscale.add("id_compagnie");
        openNewContainer("Escale", columnsEscale, db, clientDb);
        
        return columnsEscale;
            
    }
}
