/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import MongoDB.MongoDBConnexion;
import classPackage.ClientClass;
import classPackage.CompagnieClass;
import classPackage.EscaleClass;

import classPackage.PaysClass;
import classPackage.ReservationClass;
import classPackage.VolClass;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mycompany.airbabouche.compagnie.dao.container;
import static com.mycompany.airbabouche.compagnie.dao.container.openNewContainer;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import javax.swing.text.Document;

/**
 *
 * @author Flori
 */
public class Main {
    public static void main(String[] args) {
        
       
        try {
            
            PaysClass pays = new PaysClass();
            pays.containerPays();
            
            
            ClientClass client = new ClientClass();
            client.containerClient();
            
            EscaleClass escale = new EscaleClass();
            escale.containerEscale();
            
            
            VolClass vol = new VolClass();
            vol.containerVol();
            
            
            ReservationClass reservation = new ReservationClass();
            reservation.containerReservation();
            
            
           
            CompagnieClass compagnie = new CompagnieClass();
            compagnie.containerCompagnie();
            
            
            

        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(e);
        } finally {

        }
    }
    
    
}
