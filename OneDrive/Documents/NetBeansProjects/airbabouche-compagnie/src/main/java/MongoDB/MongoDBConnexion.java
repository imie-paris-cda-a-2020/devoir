/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MongoDB;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

/**
 *
 * @author Flori
 */
public class MongoDBConnexion {
    
    /**
     *
     * @return 
     */
    public MongoDatabase connexionDB(){
        MongoClient mg = new MongoClient("localhost:27017");
        MongoDatabase db = mg.getDatabase("airbabouche");
     
        return db;
    }
    
    public MongoClient getClient(){
        MongoClient mg = new MongoClient("localhost:27017");
        
     
        return mg;
    }
    
    
}
