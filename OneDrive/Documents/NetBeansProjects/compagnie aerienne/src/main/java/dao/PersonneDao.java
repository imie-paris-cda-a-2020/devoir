/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.List;
import principal.Personne;

/**
 *
 * @author Flori
 */
public interface PersonneDao {
    Personne creer();
    Personne recuperer(int id);
    Personne modifier(Personne personne);

    boolean delete(Personne personne);
    List<Personne>getAllPersonne();
}
