/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;


import java.util.List;
import principal.Vol;
/**
 *
 * @author Flori
 */
public interface VolDao {
    Vol creer();
    Vol recuperer(int id);
    Vol modifier(Vol vol);

    boolean delete(Vol vol);
    List<Vol>getAllVol();
}
