/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;


import java.util.List;
import principal.Passager;
/**
 *
 * @author Flori
 */
public interface PassagerDao {
    Passager creer();
    Passager recuperer(int id);
    Passager modifier(Passager passager);

    boolean delete(Passager passager);
    List<Passager>getAllPassager();
}
