/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;


import java.util.List;
import principal.Pays;
/**
 *
 * @author Flori
 */
public interface PaysDao {
    Pays creer();
    Pays recuperer(int id);
    Pays modifier(Pays pays);

    boolean delete(Pays pays);
    List<Pays>getAllPays();
}
